#!/usr/bin/env python
import os
import sys
import cv2
import magic
import imghdr
import argparse
import textwrap


import numpy as np


from operator import itemgetter


def get_image_paths(path, check, image_types, recurse, symlinks):
    """Searches for images on path.

    Parameters
    ----------
    path : str
        Root dir to search.
    check : callable(path)
        Callable used to determine if path is image, should return image type on success.
    image_types : [str, ...]
        List of image types to look for, return value of check is compared to these.
    recurse : bool
        Determines if search should recurse into dir's subdirs.
    symlinks : bool
        Determines if search should follow symlinks.

    Returns
    -------
    dict
        Lists of image paths mapped to their types.
    """
    # init type lists
    image_paths = {image_type: [] for image_type in image_types}
    seen = set()
    for path, dir_names, file_names in os.walk(path, followlinks=symlinks):
        # avoid infinite loop in case of symlink to parent dir
        if path in seen:
            dir_names.clear()
            continue
        seen.add(path)
        # check all files in path
        for file_name in file_names:
            file_path = os.path.join(path, file_name)
            image_type = check(file_path)
            if image_type in image_paths:
                # found image
                image_paths[image_type].append(file_path)
        # search only root dir if we're not recursing
        if not recurse:
            break
    return image_paths


def ext_check(path):
    """Checks path's extension.

    Parameters
    ----------
    path : str
        Path to check.

    Returns
    -------
    str or None
        Path's extension or None if path has no extension.
    """
    ext = os.path.splitext(path)[1]
    if ext:
        return ext[1:].lower()


def magic_check(path):
    """Checks file at path.

    Parameters
    ----------
    path : str
        Path to check.

    Returns
    -------
    str or None
        File's mime type subtype if it's type is 'image', None otherwise.
    """
    type, subtype = magic.from_file(path, mime=True).split("/")
    if type == "image":
        return subtype


def check_image_path(path, min_height, min_width, both):
    """Checks image's size.

    Parameters
    ----------
    path : str
        Path to image to check.
    min_height : int
        Minimum height for image to pass this check.
    min_width : int
        Minimum width for image to pass this check.
    both : bool
        Determines if passing both checks is required or one is enough.

    Returns
    -------
    bool or None
        None if image can't be read, True if image passes checks, False otherwise.
    """
    # check if image can be read
    image = cv2.imread(path)
    if image is None:
        return
    # it can, now check it's size
    if both:
        return image.shape[0] >= min_height and image.shape[1] >= min_width
    return image.shape[0] >= min_height or image.shape[1] >= min_width


def resize(image, height, width, interpolation):
    """Resizes image.

    If both height and width are None image is returned resized to same size, but with interpolation.
    If only one of height and width is None image is resized by keeping aspect ratio.

    Parameters
    ----------
    image : numpy.array
        Image to resize.
    height : int or None
        Height to resize image to.
    width : int or None
        Width to resize image to.
    interpolation : cv2.INTER_*
        Interpolation to pass to cv2.resize.

    Returns
    -------
    numpy.array
        Resized image.
    """
    if height is None and width is None:
        # both are None, default to image size
        height, width = image.shape[:2]
    if height is not None and width is not None:
        # both are set, resize without keeping aspect ratio
        return cv2.resize(image, (width, height), interpolation=interpolation)
    if height is not None:
        # only height is set, resize with aspect ratio intact
        return cv2.resize(image, (image.shape[1] * height // image.shape[0] or 1, height), interpolation=interpolation)
    # only width is set, resize with aspect ratio intact
    return cv2.resize(image, (width, image.shape[0] * width // image.shape[1] or 1), interpolation=interpolation)


def resize_longest(image, length, longest, interpolation):
    """Resizes image along longest or shortest edge while keeping aspect ratio.

    Parameters
    ----------
    image : numpy.array
        Image to resize.
    length : int
        Length to resize edge to.
    longest : bool
        Determines if resize should be done relative to longest or shortest edge.
    interpolation : cv2.INTER_*
        Interpolation to pass to resize.

    Returns
    -------
    numpy.array
        Resized image.
    """
    return resize(image, length, None, interpolation) if (image.shape[0] > image.shape[1]) is longest else resize(image, None, length, interpolation)


def get_hash(path):
    """Hashes resized grayscale image.

    Parameters
    ----------
    path : str
        Path to image to hash.
    """
    return hash(resize_longest(cv2.imread(path, cv2.IMREAD_GRAYSCALE), 32, True, cv2.INTER_AREA).tostring())


def hash_mark_features(path):
    """Stores hash and path of resized grayscale image.

    Parameters
    ----------
    path : str
        Path to image to hash.
    """
    hash_mark_features.similarities.setdefault(get_hash(path), []).append(path)
hash_mark_features.similarities = {}


def hash_find_similarities(path):
    """Finds images with same hash as image at path.

    Parameters
    ----------
    path : str
        Path to image to find similarities for.

    Returns
    -------
    list of tuple of int and str
        List of tuples of confidence and path of similarity.
    """
    return [(1, p) for p in hash_mark_features.similarities.get(get_hash(path), ())]


def get_hist_vector(path):
    """Computes histogram vector for image.

    Parameters
    ----------
    path : str
        Path to image.

    Returns
    -------
    numpy.array
        Histogram vector.
    """
    hist = cv2.calcHist((cv2.cvtColor(cv2.imread(path, cv2.IMREAD_COLOR), cv2.COLOR_BGR2HSV),), (0, 1, 2), None, (8, 12, 3), (0, 180, 0, 256, 0, 256))
    return cv2.normalize(hist, hist).flatten()


def get_chi_distance(a, b):
    """Computes chi distance of vectors.

    Parameters
    ----------
    a, b : numpy.array
        Vectors for computation.

    Returns
    -------
    float
        Chi distance of vectors.
    """
    return np.sum(((a - b) ** 2) / (a + b + sys.float_info.epsilon)) * 0.5


def hist_mark_features(path):
    """Stores histogram vector and path of image.

    Parameters
    ----------
    path : str
        Path to image.
    """
    hist_mark_features.vectors[path] = get_hist_vector(path)
hist_mark_features.vectors = {}


def hist_find_similarities(path):
    """Finds images similar to image at path.

    Parameters
    ----------
    path : str
        Path to image to find similarities for.

    Returns
    -------
    list of tuple of int and str
        List of tuples of confidence and path of similarity.
    """
    vector = get_hist_vector(path)
    return [(1 - get_chi_distance(v, vector), p) for p, v in hist_mark_features.vectors.items()]


def _parse_args():
    """Parses CLI args.

    Returns
    -------
    argparse.Namespace
        Namespace with parsed args.
    """
    checks = {"ext": ext_check, "magic": magic_check, "imghdr": imghdr.what}
    methods = {
        "hash": (hash_mark_features, hash_find_similarities),
        "hist": (hist_mark_features, hist_find_similarities),
    }
    ap = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter,
        description="Searches for and compares images.")
    ap.add_argument("-d", "--dir", nargs="?", const=os.path.expanduser("~"), default=os.getcwd(),
        help="dir to scan for images, defaults to CWD, flags to user's home")
    ap.add_argument("-c", "--check", default="ext", choices=checks.keys(), metavar="CHECK",
        help=textwrap.dedent("""\
            check to use to identify image types, defaults to ext, available choices:
                ext - simply check file extension
                magic - check file type based on magic, see https://github.com/ahupp/python-magic
                imghdr - check file type based on imghdr, see https://docs.python.org/3.7/library/imghdr.html"""))
    ap.add_argument("-r", "--recurse", action="store_true",
        help="recurse through dir's subdirs")
    ap.add_argument("-s", "--symlinks", action="store_true",
        help="follow symlinks")
    ap.add_argument("-t", "--type", action="append", metavar="TYPE", dest="types",
        help=textwrap.dedent("""\
            image type to look for, defaults to png and jpeg/jpg, can pass multiple
            note that defaults are used only if no TYPE is passed
            also note that TYPE interpretation depends on chosen CHECK:
                ext - file extension is checked
                magic - mime type subtype is checked
                imghdr - return of what is checked"""))
    ap.add_argument("-mh", "--min_height", default=32, type=int,
        help="images with height below this won't be processed, defaults to 32")
    ap.add_argument("-mw", "--min_width", default=32, type=int,
        help="images with width below this won't be processed, defaults to 32")
    ap.add_argument("-m", "--method", default="hash", choices=methods.keys(), metavar="METHOD",
        help=textwrap.dedent("""\
            similarity detection method to use, defaults to hash, available choices:
                hash - resizes grayscale image to 32 along the longest edge and hashes it, images with equal hashes are considered similar
                hist - computes vector of histogram of image's color distribution, similarity is determined by chi distance of vectors"""))
    ap.add_argument("-ct", "--confidence_threshold", default=0.95, type=float,
        help="confidence required for images to be considered similar, defaults to 0.95")
    args = ap.parse_args()
    # can't do this with argparse
    args.check = checks[args.check]
    args.types = args.types or ["png", "jpeg", "jpg"]
    args.mark_features, args.find_similarities = methods[args.method]
    return args


if __name__ == "__main__":
    # we're called from CLI, parse args
    args = _parse_args()
    # search for images
    image_paths = get_image_paths(args.dir, args.check, args.types, args.recurse, args.symlinks)
    # filter to only those that pass check
    image_paths = [p for ps in image_paths.values() for p in ps if check_image_path(p, args.min_height, args.min_width, True)]
    # mark features in each image
    for path in image_paths:
        args.mark_features(path)
    # store images already processed
    seen = set()
    # for each image find similarities with confidence at or above threshold
    for path in image_paths:
        seen.add(path)
        similarities = [s for s in args.find_similarities(path) if s[0] >= args.confidence_threshold and s[1] not in seen]
        if similarities:
            # got some similarities, print them sorted by confidence and path
            print("found %s similarities for %s:" % (len(similarities), path))
            for confidence, path in sorted(sorted(similarities, key=itemgetter(1)), key=itemgetter(0), reverse=True):
                print("\t%6.2f%% confidence for %s" % (confidence * 100, path))
